object SCleaner: TSCleaner
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1072' '#1074#1086#1089#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103' Steam'
  ClientHeight = 113
  ClientWidth = 289
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SSelectLang: TLabel
    Left = 8
    Top = 54
    Width = 115
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1103#1079#1099#1082' Steam:'
  end
  object CleanBlobs: TCheckBox
    Left = 8
    Top = 8
    Width = 231
    Height = 17
    Hint = #1054#1095#1080#1097#1072#1077#1090' '#1092#1072#1081#1083#1099' '#1089' '#1088#1072#1089#1096#1080#1088#1077#1085#1080#1077#1084' blob, '#1083#1077#1078#1072#1097#1080#1077' '#1074' '#1082#1072#1090#1072#1083#1086#1075#1077' '#1089#1090#1080#1084#1072
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' .blob-'#1092#1072#1081#1083#1099' '#1080#1079' '#1082#1072#1090#1072#1083#1086#1075#1072' Steam'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = CleanBlobsClick
  end
  object CleanRegistry: TCheckBox
    Left = 8
    Top = 31
    Width = 271
    Height = 17
    Hint = #1054#1095#1080#1097#1072#1077#1090' '#1079#1072#1087#1080#1089#1080' '#1089#1080#1089#1090#1077#1084#1085#1086#1075#1086' '#1088#1077#1077#1089#1090#1088#1072', '#1086#1090#1085#1086#1089#1103#1097#1080#1077#1089#1103' '#1082' Steam'
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1079#1072#1087#1080#1089#1080' Steam, '#1093#1088#1072#1085#1103#1097#1080#1077#1089#1103' '#1074' '#1088#1077#1077#1089#1090#1088#1077
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = CleanRegistryClick
  end
  object ExecuteNow: TButton
    Left = 98
    Top = 78
    Width = 94
    Height = 25
    Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100'!'
    Enabled = False
    TabOrder = 3
    OnClick = ExecuteNowClick
  end
  object SteamLanguage: TComboBox
    Left = 129
    Top = 51
    Width = 150
    Height = 21
    Hint = 
      #1042#1099#1073#1077#1088#1080#1090#1077' '#1103#1079#1099#1082' '#1080#1079' '#1089#1087#1080#1089#1082#1072', '#1082#1086#1090#1086#1088#1099#1081' '#1073#1091#1076#1077#1090' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085' '#1076#1083#1103' Steam '#1087#1086#1089#1083 +
      #1077' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1086#1095#1080#1089#1090#1082#1080
    Style = csDropDownList
    Enabled = False
    ItemHeight = 13
    ItemIndex = 1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Text = #1056#1091#1089#1089#1082#1080#1081'  (Russian)'
    Items.Strings = (
      #1040#1085#1075#1083#1080#1081#1089#1082#1080#1081' (English)'
      #1056#1091#1089#1089#1082#1080#1081'  (Russian)')
  end
  object XPColorMap1: TXPColorMap
    HighlightColor = 15921906
    BtnSelectedColor = clBtnFace
    UnusedColor = 15921906
    Left = 8
    Top = 80
  end
  object XPManifest1: TXPManifest
    Left = 40
    Top = 80
  end
end
