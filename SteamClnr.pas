(**************************************************************************
�������� ���� ��������� Steam Cleaner.

Copyright (c) 2009 - 2010 EasyCoding Team (ECTeam).
Copyright (c) 2005 - 2010 EasyCoding Team.

��������: GPL v3 (��. ���� GPL.txt).

����������� ������������ ���� ���� ��� ������������� �����
��������, �������� �� GNU GPL ������ 3.

����������� ���� EasyCoding Team: http://www.easycoding.org/
����������� �������� �������: http://www.easycoding.org/projects/steamcleaner

����� ��������� ��������� � ��������� � readme.txt,
� �������� - � license.txt � GPL.txt.
***************************************************************************)
unit SteamClnr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, LStrings, ActnMan, ActnColorMaps, XPMan;

type
  TSCleaner = class(TForm)
    CleanBlobs: TCheckBox;
    CleanRegistry: TCheckBox;
    ExecuteNow: TButton;
    SteamLanguage: TComboBox;
    SSelectLang: TLabel;
    XPColorMap1: TXPColorMap;
    XPManifest1: TXPManifest;
    procedure ExecuteNowClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CleanRegistryClick(Sender: TObject);
    procedure CleanBlobsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FullSteamPath: String;
    const AppNameGL: PAnsiChar = 'Steam Cleaner';
  end;

var
  SCleaner: TSCleaner;

implementation

{$R *.dfm}

(**********************�������� �������� ������� �� DLL*********************)
function GetSteamPath(): String;  external 'sc_core.dll';
function PKill(FileName: String): Integer; external 'sc_core.dll';
procedure CleanBlobsNow(SteamDir: String); external 'sc_core.dll';
procedure CleanRegistryNow(LangCode: Integer); external 'sc_core.dll';
(*********************��������� �������� ������� �� DLL*********************)

procedure TSCleaner.CleanBlobsClick(Sender: TObject);
begin
  if CleanBlobs.Checked or CleanRegistry.Checked then
    ExecuteNow.Enabled := True
      else
        ExecuteNow.Enabled := False
end;

procedure TSCleaner.CleanRegistryClick(Sender: TObject);
begin
  if CleanRegistry.Checked then
    SteamLanguage.Enabled := True
      else
        SteamLanguage.Enabled := False;

  if CleanRegistry.Checked or CleanBlobs.Checked then
    ExecuteNow.Enabled := True
      else
        ExecuteNow.Enabled := False
end;

procedure TSCleaner.ExecuteNowClick(Sender: TObject);
begin
  // ����������� ������������� � ������������...
  if (MessageBox(Handle, PAnsiChar(LNExecute), AppNameGL, MB_ICONQUESTION or MB_YESNO) = idYes) then
    begin
      // �������� ������...
      // ����� ��� �������� ���� ���������� ���������...
      // ���������, ������ �� ������������ ���-������...
      if (CleanBlobs.Checked or CleanRegistry.Checked) then
        begin
          if CleanBlobs.Checked then // ������� �����?
            begin
              CleanBlobsNow(FullSteamPath)
            end;
          if CleanRegistry.Checked then // ������� ������?
            begin
              CleanRegistryNow(SteamLanguage.ItemIndex)
            end;
          MessageBox(Handle, PAnsiChar(LNSeqCompleted), AppNameGL, MB_ICONINFORMATION or MB_OK);
          Application.Terminate
        end
          else
            MessageBox(Handle, PAnsiChar(LNNothingSelected), AppNameGL, MB_ICONINFORMATION or MB_OK)
    end
end;

procedure TSCleaner.FormCreate(Sender: TObject);
begin
  // ������� ������� Steam.exe ���� �� �������...
  if (PKill('Steam.exe') <> 0) then
    Application.MessageBox(PAnsiChar(LNProcessDetected), AppNameGL, MB_ICONWARNING or MB_OK);

  // ��������� ��������� ���� �� ������ ����� Windows...
  Application.Title := SCleaner.Caption;

  // ���������� ��� Steam ���������� � �������� ���� � ����...
  try
    // �������� ���� � Steam...
    FullSteamPath := GetSteamPath;
  except
    // Steam �� ���������.
    MessageBox(Handle, PAnsiChar(LNSteamNotDetected), AppNameGL, MB_ICONERROR or MB_OK);
    Application.Terminate // ��������� ������ ����������...
  end
end;

end.
