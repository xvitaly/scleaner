(**************************************************************************
���� ��������� Steam Cleaner.

Copyright (c) 2009 - 2010 EasyCoding Team (ECTeam).
Copyright (c) 2005 - 2010 EasyCoding Team.

��������: GPL v3 (��. ���� GPL.txt).

����������� ������������ ���� ���� ��� ������������� �����
��������, �������� �� GNU GPL ������ 3.

����������� ���� EasyCoding Team: http://www.easycoding.org/
����������� �������� �������: http://www.easycoding.org/projects/steamcleaner

����� ��������� ��������� � ��������� � readme.txt,
� �������� - � license.txt � GPL.txt.
***************************************************************************)
library sc_core;

uses
  Windows, SysUtils, Classes, Tlhelp32, Registry;

{$R *.res}

(********************������ �������� ����********************
***********************��������: GPLv3***********************
****************�������� ������ �������: SQL.RU**************)
function PKill(FileName: String): Integer;
const
  PROCESS_TERMINATE=$0001;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32; 
begin 
  result := 0; 

  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := Sizeof(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);

  while integer(ContinueLoop) <> 0 do
    begin
      if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
         UpperCase(FileName))
            or (UpperCase(FProcessEntry32.szExeFile) =
              UpperCase(FileName))) then
                  Result := Integer(TerminateProcess(OpenProcess(
                        PROCESS_TERMINATE, BOOL(0),
                        FProcessEntry32.th32ProcessID), 0));
      ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32)
    end;
  CloseHandle(FSnapshotHandle)
end;
(********************����� �������� ����********************)

function GetSteamPath(): String;
var Reg: TRegistry;
begin
  Result := ''; // ��������� ����� �� ���� Undefined Result...
  Reg := TRegistry.Create; // ���������� ������...
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.KeyExists('Software\Valve\Steam') then
      begin
        Reg.OpenKeyReadOnly('Software\Valve\Steam');
        if Reg.ValueExists('InstallPath') then
          Result := IncludeTrailingPathDelimiter(Reg.ReadString('InstallPath'))
            else
              raise ERegistryException.Create('Exception: No InstallPath value detected! Please run Steam.');
        Reg.CloseKey
      end
        else
          raise ERegistryException.Create('Exception: Key HKLM/Software/Valve/Steam does not exist! Please run Steam!');
  finally
    Reg.Free
  end
end;

procedure CleanBlobsNow(SteamDir: String);
var FilePath: String;
begin
  // �������� ��������...
  FilePath := SteamDir + 'AppUpdateStats.blob'; // ���������� ��� ������� ��������� �� ��������...
  DeleteFile(FilePath); // �������...
  FilePath := SteamDir + 'ClientRegistry.blob'; // ...�������...
  DeleteFile(FilePath) // �������...
end;

procedure CleanRegistryNow(LangCode: Integer);
var Reg: TRegistry;
    XLang: String;
begin
  // ���������� ������...
  Reg := TRegistry.Create;
  try
    // ������������ �������� ���� ���������� �������...
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    // ���� ������ ��� ���� ����������, ������� ��� �� �������...
    if Reg.KeyExists('Software\Valve') then
      Reg.DeleteKey('Software\Valve');
    // ��������� �� �� ����� �������� � ��� HKCU...
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.KeyExists('Software\Valve') then
      Reg.DeleteKey('Software\Valve');
    // ������ ���� ��� ������ �������� ����� � ������...
    Reg.OpenKey('Software\Valve\Steam', True);
    case LangCode of
      0: XLang := 'english';
      1: XLang := 'russian';
      else
        XLang := 'english'
    end;
    Reg.WriteString('language', XLang)
  finally
    Reg.CloseKey;
    Reg.Free
  end
end;

(************************������ �������� �������**************************)
exports PKill, GetSteamPath, CleanBlobsNow, CleanRegistryNow;
(*************************����� �������� �������**************************)

begin
end.
