(**************************************************************************
���������� ������ ��������� Steam Cleaner.

Copyright (c) 2009 - 2010 EasyCoding Team (ECTeam).
Copyright (c) 2005 - 2010 EasyCoding Team.

��������: GPL v3 (��. ���� GPL.txt).

����������� ������������ ���� ���� ��� ������������� �����
��������, �������� �� GNU GPL ������ 3.

����������� ���� EasyCoding Team: http://www.easycoding.org/
����������� �������� �������: http://www.easycoding.org/projects/steamcleaner

����� ��������� ��������� � ��������� � readme.txt,
� �������� - � license.txt � GPL.txt.
***************************************************************************)
program sc_console;

{$APPTYPE CONSOLE}

uses
  SysUtils;

var
  FullSteamPath: String;
  SteamLangID: Integer;
  InpBtn: Char;

(**********************�������� �������� ������� �� DLL*********************)
function GetSteamPath(): String;  external 'sc_core.dll';
function PKill(FileName: String): Integer; external 'sc_core.dll';
procedure CleanBlobsNow(SteamDir: String); external 'sc_core.dll';
procedure CleanRegistryNow(LangCode: Integer); external 'sc_core.dll';
(*********************��������� �������� ������� �� DLL*********************)

begin
  // ������� �����������...
  Writeln('###########################################################################');
  Writeln('#            WELCOME TO STEAMCLEANER (CONSOLE VERSION)                    #');
  Writeln('#   This program can fix all known Steam problems. Follow instructions.   #');
  Writeln('#                                                                         #');
  Writeln('#        (C) 2005 - 2010 EasyCoding Team. All rights reserved.            #');
  Writeln('#    Original author: V1TSK (vitaly@easycoding.org). ICQ: 298024135.      #');
  Writeln('#                                                                         #');
  Writeln('#    Official site: http://www.easycoding.org/projects/steamcleaner       #');
  Writeln('#   Source code available here: http://code.google.com/p/steamcleaner/    #');
  Writeln('###########################################################################');
  Writeln;

  // ������� ���� � Steam...
  try
    FullSteamPath := GetSteamPath();
  except
    // ��������� ����������� ������, ��������� ������ ����������...
    Writeln('WARNING: Steam was not detected! Please, run Steam, exit it and then rerun SteamCleaner!');
    Writeln;
    Writeln('Application crashed. Press ENTER to close this window!');
    Readln;
    halt
  end;

  // �������� ������...
  if (FindCmdLineSwitch('blobs') or FindCmdLineSwitch('registry')) then
    begin
      // ������� ���������...
      Writeln('OK. Available parametres detected. Let''s rock!');
      Writeln;

      // �������� ����� Steam �� ��� �������...
      Write('Detecting Steam.exe in memory...');
      if (PKill('Steam.exe') <> 0) then
        begin
          Writeln(' DETECTED!');
          Writeln('Notice: Steam.exe detected! Steam.exe was successfully terminated!')
        end
          else
            Writeln(' Not detected. Ready to start cleanup.');

      // ������� �������� � ������������ ������������� �������...
      Repeat
        Writeln;
        Write('Start cleanup now? (y/n) ');
        Readln(InpBtn);
        try
          if (InpBtn = 'y') then
      	    begin
              // ������������� ��������, ����� ��������...
              if FindCmdLineSwitch('blobs') then
                begin
                  // ����� ������� �����...
                  Writeln;
                  Write('Cleaning blobs...');
                  CleanBlobsNow(FullSteamPath); // ������� �����...
                  Writeln(' Done.');
                  Writeln('Blob files from Steam directory was successfully removed!')
                end;
              if FindCmdLineSwitch('registry') then
                begin
                  // ����� ������� ������...
                  Writeln;
                  Writeln('Preparing to clean Steam data from registry...');
                  Repeat
                    Writeln('Please select Steam language from list:');
                    Writeln('  0 - English (USA/UK);');
                    Writeln('  1 - Russian (Russian Federation).');
                    Write('Enter your choise (0-1): ');
                    try
                      Readln(SteamLangID)
                    except
                      // ������������ ��������� ����������...
                      SteamLangID := 0; // ������������� ���������� ����...
                      Writeln('Warning: incorrect number entered. Steam language will be English!');
                      Writeln
                    end;
                  Until SteamLangID in [0..1];
                  Writeln;
                  Write('Cleaning registry...');
                  CleanRegistryNow(SteamLangID); // ������ ������...
                  Writeln(' Done.');
                  Writeln('Steam settings stored in Windows registry was successfully removed!');
               end;
           end;
        except
          // ������������ ��������� ����������...
          InpBtn := 'n'
        end
      Until (InpBtn in ['y','n']) or (InpBtn = #26);
    end
      else
        begin
          Writeln('This is a console application. Run it with command-line parameters.');
          Writeln;
          Writeln('Available parameters:');
          Writeln('sc_console.exe [-blobs] [-registry]');
          Writeln(' -blobs     SteamCleaner will clean all .blob files in Steam directory.');
          Writeln(' -registry  Steam Cleaner will clean all Steam data stored in registry.')
        end;

  // ������� ������ ������...
  Writeln;

  // ������� ��������� �� ��������� ������ ����������...
  Writeln('Application finished. Press ENTER to exit!');

  // �� ��������� ���������� ������� ����...
  Readln
end.
